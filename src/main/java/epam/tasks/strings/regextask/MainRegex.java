package epam.tasks.strings.regextask;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainRegex {

    public MainRegex() {}

    public void showRegex(){
        String fileName = "regexdata.txt";
        List<String> list = new ArrayList<>();

        String regEx = "([A-Z])(.*)[\\.]";

        Predicate<String> textFilter = Pattern
                .compile(regEx)
                .asPredicate();

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            list = stream
                    .filter(textFilter)
                    .collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }

        list.forEach(System.out::println);
    }

    public void splitRegex() {
        String fileName = "regexdata.txt";
        List<String> list = new ArrayList<>();
        String file = null;
        // Open this file.
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new FileReader(fileName));

            while (true) {
                // Read lines from file.
                String line = reader.readLine();
                file += line;
                if (line == null) {
                    break;
                }
            }

            Pattern p = Pattern.compile("the|you");
            String partsLile = file.trim().replaceAll("[\r]+", " ");

            // Split line on ...
            String[] parts = p.split(partsLile);
            for (String part : parts) {
                System.out.println(part);
            }
            System.out.println();

            reader.close();
            //replace vowels
            String partsLile2 = file.replaceAll("[aeiou]", "_");
            System.out.println(partsLile2);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
