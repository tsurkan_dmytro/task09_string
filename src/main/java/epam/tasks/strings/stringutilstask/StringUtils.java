package epam.tasks.strings.stringutilstask;

import java.lang.reflect.Field;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StringUtils {

    private final Logger logger = LogManager.getLogger(StringUtils.class.getName());

    public StringUtils() {
    }

    public void getClassParamData(Class class_param){

        Field[] fields = class_param.getFields();

        String param = "";
        int i = 0;
        for (Field field: fields) {
            if( i%4 == 0){
                param += " \n";
            }
            i++;
            param += field + "  <<; ";
        }
        logger.info(param);

    }
}
