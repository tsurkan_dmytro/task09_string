package epam.tasks.strings.internacionalis;

import epam.tasks.strings.internacionalis.langI.Printable;
import epam.tasks.strings.regextask.MainRegex;

import java.util.*;

public class MenuPart {

    private static final String RES_URI = "epam.tasks.strings.internacionalis.langsres.MyResources";

    private Map<String,String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    private ResourceBundle bundle;
    private Locale loc ;

    public MenuPart() {

        loadLocaleEn();

        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - Eng menu");
        menu.put("2", " 2 - Croatian menu");
        menu.put("3", " 3 - Danish menu");
        menu.put("4", " 4 - Show regex");
        menu.put("5", " 5 - Split regex(the|you) and replace vowels");
        menu.put("Q", " Q - exit");

        methodsMenu.put("1", this::engmenu);
        methodsMenu.put("2", this::croatianmenu);
        methodsMenu.put("3", this::danishmenu);
        methodsMenu.put("4", this::showRegex);
        methodsMenu.put("5", this::splitRegex);
    }

    private void loadLocaleEn() {
        loc = new Locale("en", "EN");
        bundle = ResourceBundle.getBundle(RES_URI, loc);
    }

    private void updateLanguage() {

        System.out.println("Name: " + bundle.getObject("name"));
        System.out.println("Lang_menu: " + bundle.getObject("lang_menu"));
        System.out.println("Task: " + bundle.getObject("task"));
        System.out.println("Menu name: " + bundle.getObject("menu_name"));
        System.out.println("Text: " + bundle.getObject("description"));

    }

    private void outputMenu(){
        System.out.println("\n" + bundle.getObject("menu"));

        for (String str: menu.values()){
            System.out.println(str);
        }
    }

    public void show(){
        String keyMenu = null;

        loadLocaleEn();

        do {
            outputMenu();
            System.out.println(bundle.getObject("menu_name"));
            keyMenu = input.nextLine().toUpperCase();
            try{
                methodsMenu.get(keyMenu).print();
            }catch (Exception e){
                System.out.println(bundle.getObject("menu_problem"));
            }
        }while (!keyMenu.equals("Q"));
    }

    private void engmenu(){
        Locale loc = new Locale("en", "EN");
        bundle = ResourceBundle.getBundle(RES_URI, loc);
        updateLanguage();
    }
    private void croatianmenu(){
        Locale loc = new Locale("hr", "HR");
        bundle = ResourceBundle.getBundle(RES_URI, loc);
        menu.put("2", " 2 - Croatian menues");
        updateLanguage();
    }
    private void danishmenu(){
        Locale loc = new Locale("da", "DA");
        bundle = ResourceBundle.getBundle(RES_URI, loc);
        menu.put("3", " 3 - Danish mune");
        updateLanguage();
    }
    private void showRegex(){
        MainRegex regex = new MainRegex();
        regex.showRegex();
    }

    private void splitRegex(){
        MainRegex regex = new MainRegex();
        regex.splitRegex();
    }

}
