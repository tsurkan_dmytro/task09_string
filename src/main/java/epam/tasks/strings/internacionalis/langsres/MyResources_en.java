package epam.tasks.strings.internacionalis.langsres;

import java.util.ListResourceBundle;

public class MyResources_en extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        return resources;
    }

    private final Object[][] resources = {

            {"name", "Some hoistory text"},
            {"lang_menu", "English"},
            {"task", "About localisation"},
            {"menu_name", "Select menu:"},
            {"menu_problem", "Menu problem"},
            {"menu", "Menu:"},
            {"description", "English is a West Germanic language that originated from Anglo-Frisian dialects brought to Britain in the mid 5th to 7th centuries AD by Anglo-Saxon settlers.\n" +
                    " With the end of Roman rule in 410 AD, Latin ceased to be a major influence on the Celtic languages spoken by the majority of the\n population.[citation needed] People from what is now northwest Germany, \n" +
                    "west Denmark and the Netherlands settled in the British Isles from the mid-5th century and came to culturally dominate the bulk of southern Great Britain until the 7th century. The Anglo-Saxon language,\n" +
                    " now called Old English, originated as a group of Anglo-Frisian dialects which were spoken, at least by the settlers, in England and southern and eastern Scotland in the early Middle Ages. It displaced to some \n" +
                    "extent the Celtic languages that predominated previously. Old English also reflected the varied origins of the Anglo-Saxon kingdoms established in different parts of Britain. The Late West Saxon dialect eventually became dominant.\n" +
                    " A significant subsequent influence on the shaping of Old English came from contact with the North Germanic languages spoken by the Scandinavian Vikings who conquered and colonized parts of \n" +
                    "Britain during the 8th and 9th centuries, which led to much lexical borrowing and grammatical simplification.\n" +
                    " The Anglian dialects had a greater influence on Middle English. "
                    }
    };
}
