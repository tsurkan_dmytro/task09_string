package epam.tasks.strings.internacionalis.langI;

@FunctionalInterface
public interface Printable {
    public void print();
}
