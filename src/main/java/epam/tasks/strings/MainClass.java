package epam.tasks.strings;

import epam.tasks.strings.internacionalis.MenuPart;
import epam.tasks.strings.stringutilstask.StringUtils;
import epam.tasks.strings.stringutilstask.Test1;
import epam.tasks.strings.stringutilstask.Test2;

public class MainClass {
    public static void main(String[] args) {
        StringUtils stringUtils = new StringUtils();
        stringUtils.getClassParamData(Test1.class);
        stringUtils.getClassParamData(Test2.class);

        MenuPart menu = new MenuPart();
        menu.show();

    }
}
