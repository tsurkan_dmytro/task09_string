package epam.tasks.strings.bigtask;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UtilHelper {
    String fileName = "bigtaskdata.txt1";

    private List<Sentence> sentencesObjects;

    private Sentence sentence;
    private List<String> listWithoutSpaces;

    public UtilHelper() {
        sentencesObjects = new ArrayList<>();
       // this.sentence = new Sentence();
    }

    public void createFileNoTabs(){

        listWithoutSpaces = new ArrayList<>();

        String regEx = "([A-Z])(.*)[.!?;]";

        Predicate<String> textFilter = Pattern
                .compile(regEx)
                .asPredicate();

        try (Stream<String> lines2 = Files.lines(Paths.get(fileName))) {
            listWithoutSpaces = lines2
                    .map(line -> line.replaceAll("\\s+|\\t+|\\n", " "))
                    .filter(textFilter)
                    .collect(Collectors.toList());
            Files.write(Paths.get(fileName+1), listWithoutSpaces);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadAllSentences(){
        String[] output = createArrayFromFile();
        String correctPart = null;

        for (String pe : output) {
            correctPart = pe.replaceAll("\\s+|\\t+|\\n+", " ");

            int tempWordsCount = countWordsUsingSplit(correctPart);
            int tempWordsDublicate = findDublicateWords(correctPart);

            Sentence sentence = new Sentence();
            sentence.setWordsCount(tempWordsCount);
            sentence.setName(correctPart);
            sentence.setCountDublicate(tempWordsDublicate);
            sentencesObjects.add(sentence);
        }
    }

    public int findDublicateWords(String string ) {
        int countDublicate=1;
        string = string.toLowerCase();

        //Split the string into words using built-in function
        String words[] = string.split(" ");

        for (int i = 0; i < words.length; i++) {

            for (int j = i + 1; j < words.length; j++) {
                if (words[i].equals(words[j])) {
                    countDublicate++;
                    //System.out.println(countDublicate);
                    //Set words[j] to 0 to avoid printing visited word
                    words[j] = "0";
                }
            }
        }
        return countDublicate;
    }

    public int countWordsUsingSplit(String input) {
        if (input == null || input.isEmpty()) {
            return 0;
        }
        String[] words = input.split("\\s+");
        return words.length;
    }

    public String[] createArrayFromFile() {

        String data = "";
        try {
            data = new String(Files.readAllBytes(Paths.get(fileName)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] output = data.split("\\.");

        return output;
    }

    public void showAllSentences() {
         for(Sentence p: sentencesObjects){
             System.out.println(p.getName());
         }
    }

    public void showWithDublicateWords() {
         for(Sentence p: sentencesObjects){
             if(p.getCountDublicate()>=2){
                System.out.println(p.getName()+" >");
             }
         }
    }

    public void showSortedWords() {
        Collections.sort(sentencesObjects);
         for(Sentence p: sentencesObjects){
             System.out.println(p.getName());
         }
    }
}
