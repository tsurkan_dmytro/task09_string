package epam.tasks.strings.bigtask;

import java.util.ArrayList;
import java.util.List;

public class Sentence implements Comparable<Sentence> {

    private String name;

    int wordsCount;
    int countDublicate;

    private List<String> sentencesList = new ArrayList<>();

    public Sentence() {   }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSentencesList() {
        return sentencesList;
    }

    public void setSentencesList(List<String> sentencesList) {
        this.sentencesList = sentencesList;
    }

    public void showSentences(List<String> sentences) {
        for (String s: sentences) {
            System.out.println(s);
        }
    }

    public int getWordsCount() {
        return wordsCount;
    }

    public void setWordsCount(int wordsCount) {
        this.wordsCount = wordsCount;
    }

    public int getCountDublicate() {
        return countDublicate;
    }

    public void setCountDublicate(int countDublicate) {
        this.countDublicate = countDublicate;
    }

    @Override
    public int compareTo(Sentence sentence) {
        if (this.wordsCount == sentence.wordsCount) {
            return 0;
        } else if (this.wordsCount < sentence.wordsCount) {
            return -1;
        } else {
            return 1;
        }
    }
}