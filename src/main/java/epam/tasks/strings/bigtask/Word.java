package epam.tasks.strings.bigtask;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Word {

    private String name;
    int count;

    private Map<Integer, String> wordsFromList = new HashMap<>();

    private List<Word> words = new ArrayList<>();

    public Word() {

    }

    /*String word = Arrays.stream(input.split(" "))
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
            .entrySet().stream()
            .max(Map.Entry.comparingByValue())
            .map(Map.Entry::getKey)
            .orElse(null);

    //System.out.println("Most popular word is " + word);*/

    public void countWords() {

        long wordCount = 0;
        Path textFilePath = Paths.get("C:\\JavaBrahman\\WordCount.txt");
        try

        {
            Stream<String> fileLines = Files.lines(textFilePath, Charset.defaultCharset());
            wordCount = fileLines.flatMap(line -> Arrays.stream(line.split(" "))).count();
        } catch(IOException ioException)
            {
                ioException.printStackTrace();
            }
        System.out.println("Number of words in WordCount.txt: "+wordCount);
}
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Word> getWords() {
        return words;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    public Map<Integer, String> getWordsFromList() {
        return wordsFromList;
    }

    public void setWordsFromList(Map<Integer, String> wordsFromList) {
        this.wordsFromList = wordsFromList;
    }
}
